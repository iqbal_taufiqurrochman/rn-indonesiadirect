import * as React from 'react';
import { View, Text , Platform, UIManager} from 'react-native';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import store from './src/redux/store';

import AppRouteConfig from './AppRouteConfig';

if (Platform.OS === 'android') {
	if (UIManager.setLayoutAnimationEnabledExperimental) {
		UIManager.setLayoutAnimationEnabledExperimental(true);
	}
}

function App() {
	return (
		<Provider store={store}>
			<NavigationContainer>
				<AppRouteConfig />
			</NavigationContainer>
		</Provider>
	);
}

export default App;