import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import BottomTab from '../component/BottomTab';
import Header from '../component/Header';
import HorizontalMenu from '../component/HorizontalMenu';
import ProductDefault from '../component/Product/Default';
import Slider from '../component/Slider';
import store from '../redux/store';
import Color from '../util/Color';
import { HttpRequest } from '../util/Http';

export default class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            category: [],
            home: [],
            isLoading: true
        }
    }

    componentDidMount() {
        this.getHome()

        // console.log("cart", store.getState().cart)
    }

    async getHome() {
        try {
            let res = await HttpRequest.getAllJson();
            this.setState({
                category: res.data.category,
                home: res.data.home,
                isLoading: false
            })
        } catch (error) {
            console.log(error)
            this.setState({ isLoading: false })
        }
    }

    async logout() {

        try {
            await AsyncStorage.removeItem("@transactions", null)

            let root = store.getState().root;
            root.saveAuth(null);
        } catch (error) {
            console.log("error", error)
        }
    }

    render() {
        let { category, home, isLoading } = this.state
        return (
            <View style={{ flex: 1, backgroundColor: 'transparent' }}>

                <View style={styles.header} >
                    <Image resizeMode="contain" source={require('../assets/logo.png')} style={styles.logo} />
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', flex: 1 }}>
                        {/* <TouchableOpacity activeOpacity={0.4}>
                            <Ionicons style={{ marginHorizontal: 10 }} name="ios-notifications-outline" size={25} />
                        </TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Cari")} activeOpacity={0.4}>
                            <Ionicons style={{}} name="ios-search-outline" size={25} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            this.logout()
                        }} activeOpacity={0.4}>
                            <MaterialCommunityIcons style={{}} name="logout" size={25} style={{marginLeft : 15}} />
                        </TouchableOpacity>
                    </View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false}>

                    {!isLoading && (
                        <>
                            <View style={{ marginTop: 20 }} />
                            <Slider />

                            <HorizontalMenu
                                containerStyle={{ margin: 15 }}
                                label="Category"
                                items={
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        {category?.map((item, key) => {
                                            return (
                                                <TouchableOpacity
                                                    key={key}
                                                    onPress={() => this.props.navigation.navigate("Cari", { item })}
                                                    style={{ alignItems: 'center', justifyContent: 'center', marginRight: 30 }}>
                                                    <Image source={{ uri: item.icon }} style={{ width: 40, height: 40 }} resizeMode="contain" />
                                                    <Text style={{ marginTop: 5 }}>{item.name}</Text>
                                                </TouchableOpacity>
                                            )
                                        })}
                                    </View>
                                }
                            />

                            {home?.map((item, key) => {

                                return (
                                    <HorizontalMenu
                                        key={key}
                                        containerStyle={{ margin: 15 }}
                                        label={item.title}
                                        items={
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                {item?.data?.map((i, index) => {
                                                    return (
                                                        <ProductDefault
                                                            key={index}
                                                            source={{ uri: i.images[0] }}
                                                            item={i}
                                                            onClick={() => {
                                                                this.props.navigation.navigate("ProductDetail", { item: i })
                                                            }}
                                                            containerStyle={{
                                                                marginRight: 10,
                                                                marginBottom: 20, backgroundColor: Color.white,
                                                                elevation: 2, borderRadius: 8,
                                                                padding: 5
                                                            }}
                                                        />
                                                    )
                                                })}
                                            </View>
                                        }
                                    />
                                )
                            })}
                        </>
                    )}

                    {isLoading && (
                        <ActivityIndicator size="large" color={Color.primary} />
                    )}

                </ScrollView>

                <BottomTab
                    selected={0}
                    onClick={(event) => this.props.navigation.navigate(event)}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15, paddingVertical: 10,
    },
    logo: {
        height: 50,
        width: 50,
    }
})