import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, RefreshControl } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Button from '../component/Button';
import ProductInCart from '../component/Product/InCart';
import Color from '../util/Color';
import Rupiah from '../util/Rupiah';

class Transaction extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            orders: null,
            isLoading: true
        }
    }

    componentDidMount() {
        this.props.navigation.addListener("focus", () => {
            this.getAllOrders()
        })
        this.getAllOrders()
    }

    async getAllOrders() {
        let data = await AsyncStorage.getItem("@transactions")
        this.setState({
            orders: JSON.parse(data),
            isLoading: false
        })
        console.log("data", JSON.parse(data))
    }

    render() {
        let { orders, isLoading } = this.state
        return (
            <View style={styles.main}>
                <View style={styles.header}>
                    <TouchableOpacity activeOpacity={0.4} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back-outline" color={Color.white} size={25} />
                    </TouchableOpacity>

                    <Text style={{ color: Color.white, fontSize: 18, fontWeight: 'bold', marginHorizontal: 10 }}>My Orders</Text>
                </View>

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            onRefresh={() => this.getAllOrders()}
                            refreshing={isLoading}
                        />}
                >

                    {this.state.orders == null && (
                        <Text style={{ margin: 20, fontSize: 14, fontStyle: 'italic', textAlign: 'center' }}>(Tidak ada riwayat transaksi)</Text>
                    )}

                    {!isLoading && (
                        this.state.orders != null && (
                            this.state.orders?.map((item, key) => {
                                return (
                                    <View key={key} style={styles.content}>

                                        <View style={{ marginVertical: 8, paddingVertical: 8, borderBottomColor: Color.gray, borderBottomWidth: 1.5, }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                                <Text style={{ fontWeight: "bold", fontSize: 14 }}>Alamat Lengkap</Text>
                                            </View>
                                            <Text style={{}}>{item.alamat}</Text>
                                        </View>

                                        {item.orders.map((i, index) => {
                                            return (
                                                <ProductInCart
                                                    key={index}
                                                    item={i}
                                                    itemInCart={i.quantity}
                                                    editable={false}
                                                    containerStyle={{ marginBottom: 8 }}
                                                />
                                            )
                                        })}

                                        <View style={{ marginVertical: 8, paddingVertical: 8, borderTopColor: Color.gray, borderTopWidth: 1.5, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                            <Text style={{ fontWeight: "bold", fontSize: 16 }}>Total Transaksi</Text>
                                            <Text style={{ fontWeight: "bold" }}>{Rupiah.format(item.total)}</Text>
                                        </View>
                                    </View>
                                )
                            })
                        )
                    )}
                </ScrollView>


            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    header: {
        height: 80,
        backgroundColor: Color.primary,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    content: {
        // borderBottomColor: Color.grayText,
        // borderBottomWidth: 1,
        backgroundColor: Color.white,
        elevation: 2,
        borderRadius: 8,
        paddingHorizontal: 15,
        marginTop: 15,
        paddingVertical: 10,
        marginHorizontal: 20
    }
})

export default Transaction