import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Alert } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Button from '../component/Button';
import InputWithLabel from '../component/InputWithLabel';
import Color from '../util/Color';
import AsyncStorage from '@react-native-community/async-storage';

class AddAddress extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            label: '',
            alamat_lengkap: '',
            nama_penerima: '',
            hp: ''
        }
    }

    async addAddress() {
        let { label, alamat_lengkap, nama_penerima, hp } = this.state
        if (label == "") {
            Alert.alert("Mohon isi label alamat")
            return
        }
        if (alamat_lengkap == "") {
            Alert.alert("Mohon isi alamat lengkap")
            return
        }
        if (nama_penerima == "") {
            Alert.alert("Mohon isi nama penerima")
            return
        }
        if (hp == "") {
            Alert.alert("Mohon isi Nomor hp")
            return
        }

        let address = await AsyncStorage.getItem("@address")
        let data = {
            label, alamat_lengkap, nama_penerima, hp
        }

        if (address != null) {
            let result = address.push(data)
            console.log("result1", result)
            await AsyncStorage.setItem("@address", JSON.stringify(result))
        } else {
            let newAddress = []
            let result = newAddress.push(data)
            console.log("result2", result)
            await AsyncStorage.setItem("@address", JSON.stringify(result))
        }

        Alert.alert("Save Address", "Save address success")
        this.props.navigation.goBack()
    }

    render() {
        let { label, alamat_lengkap, nama_penerima, hp } = this.state
        return (
            <View style={styles.main}>
                <View style={styles.header}>
                    <TouchableOpacity activeOpacity={0.4} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back-outline" color={Color.white} size={25} />
                    </TouchableOpacity>

                    <Text style={{ color: Color.white, fontSize: 18, fontWeight: 'bold', marginHorizontal: 10 }}>Add Address</Text>
                </View>

                <ScrollView>

                    <View style={{ margin: 20 }}>
                        <InputWithLabel
                            label="Label Alamat"
                            placeholderTextColor={Color.gray}
                            placeholder="Rumah..."
                            value={label}
                            onChangeText={(i) => this.setState({ label: i })}
                        />
                        <InputWithLabel
                            label="Alamat Lengkap"
                            placeholderTextColor={Color.gray}
                            placeholder="Gayungsari Timur 123, Surabaya"
                            value={alamat_lengkap}
                            onChangeText={(i) => this.setState({ alamat_lengkap: i })}
                        />
                        <InputWithLabel
                            label="Nama Penerima"
                            placeholderTextColor={Color.gray}
                            placeholder="penerima..."
                            value={nama_penerima}
                            onChangeText={(i) => this.setState({ nama_penerima: i })}
                        />
                        <InputWithLabel
                            label="No Hp"
                            placeholderTextColor={Color.gray}
                            placeholder="081xxxxx"
                            keyboardType="number-pad"
                            value={hp}
                            onChangeText={(i) => this.setState({ hp: i })}
                        />
                    </View>
                </ScrollView>

                <Button
                    onPress={() => this.addAddress()}
                    style={{ height: 70, borderRadius: 0 }}>
                    Save Address
                </Button>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    header: {
        height: 80,
        backgroundColor: Color.primary,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
})

export default AddAddress