import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions, TextInput, FlatList, ActivityIndicator, LayoutAnimation } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Header from '../component/Header';
import HorizontalMenu from '../component/HorizontalMenu';
import ProductDefault from '../component/Product/Default';
import Slider from '../component/Slider';
import store from '../redux/store';
import Color from '../util/Color';
import { HttpRequest } from '../util/Http';

const category = [{}, {}, {}, {}, {}]
const { width, height } = Dimensions.get('screen')
export default class Cari extends Component {

    constructor(props) {
        super(props)
        this.state = {
            products: [],
            productsView: [],
            categories: [],
            selectedCategory: "",
            isLoading: false,
            search: "",
            params: null
        }
    }

    componentDidMount() {
        // this.getAllCategory()
        // this.getAllProducts()
        this.getAllJson()
    }

    checkParams() {
        let params = this.props.route?.params?.item
        if (params) {
            this.setState({
                params,
                selectedCategory: params?.id
            }, this.filterProduct)
        }
    }

    async getAllJson() {
        try {
            let res = await HttpRequest.getAllJson()
            let semua = [{
                id: "",
                name: "Semua"
            }]
            this.setState({
                categories: semua.concat(res.data.category),
                isLoading: false,
                products: res.data.products,
                productsView: res.data.products
            }, this.checkParams)
        } catch (error) {
            console.log(error)
        }
    }

    async getAllCategory() {
        try {
            let res = await HttpRequest.getCategory()
            let semua = [{
                id: "",
                name: "Semua"
            }]
            this.setState({
                categories: semua.concat(res.data)
            })
        } catch (error) {
            console.log("error", error)
            this.setState({ isLoading: false })
        }
    }

    async getAllProducts() {
        try {
            let res = await HttpRequest.getProduct()
            this.setState({
                isLoading: false,
                products: res.data,
                productsView: res.data
            })
        } catch (error) {
            console.log("error", error)
            this.setState({ isLoading: false })
        }
    }

    searchProduct() {
        let { productsView, products, search } = this.state
        let filtered = []

        if (search == "") {
            this.filterProduct()
        } else {
            productsView.forEach((item) => {
                if (item.name.toLowerCase().search(search.toLocaleLowerCase()) != -1) {
                    filtered.push(item)
                }
            })
            this.setState({ productsView: filtered })
        }
    }

    filterProduct() {
        let { selectedCategory, search, products } = this.state
        let productFiltered = []
        products.forEach((item) => {
            if (selectedCategory != "") {
                if (item.category.id == selectedCategory) {
                    productFiltered.push(item)
                }
            } else {
                productFiltered.push(item)
            }
        })
        console.log("products", products)
        console.log("filtered", productFiltered)
        this.setState({
            productsView: productFiltered
        })
        // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    }

    render() {
        let { navigations } = this.props
        let { products, productsView, categories, selectedCategory, isLoading } = this.state
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={styles.header}>
                    <TouchableOpacity activeOpacity={0.4} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back-outline" color={Color.white} size={25} style={{ marginTop: 10 }} />
                    </TouchableOpacity>

                    <View style={styles.searchContainer}>
                        <TextInput
                            onChangeText={(search) => this.setState({ search }, this.searchProduct)}
                            style={styles.search}
                            placeholder="Search..."
                        />
                        {isLoading && (
                            <ActivityIndicator size="small" color={Color.primary} />
                        )}
                    </View>
                </View>

                <View style={{ marginTop: 30 }} />

                <HorizontalMenu
                    containerStyle={{ paddingLeft: 20, marginBottom: 20 }}
                    items={
                        <>
                            {categories.map((item, key) => {
                                let selected = item.id == selectedCategory
                                return (
                                    <TouchableOpacity
                                        key={key}
                                        onPress={() => this.setState({ selectedCategory: item.id }, this.filterProduct)}
                                        style={selected ? styles.activeCategory : styles.category}
                                    >
                                        <Text style={{ color: Color.primary }} >{item.name}</Text>
                                    </TouchableOpacity>
                                )
                            })}
                        </>
                    }
                />

                {productsView.length == 0 && (
                    <Text style={{ textAlign: 'center', fontSize: 18, fontStyle: 'italic' }}>(Tidak ada produk)</Text>
                )}
                <FlatList
                    style={{ marginLeft: 8 }}
                    data={productsView}
                    // contentContainerStyle={{ alignItems: "flex" }}
                    renderItem={({ item }) => {
                        return (
                            <ProductDefault
                                onClick={() => this.props.navigation.navigate("ProductDetail", { item })}
                                source={{ uri: item.images[0] }}
                                item={item}
                                itemSize={(width / 2) - 38}
                                containerStyle={{
                                    marginLeft: 10, marginRight: 10,
                                    marginBottom: 20, backgroundColor: Color.white,
                                    elevation: 2, borderRadius: 8,
                                    padding: 5
                                }}
                            />
                        )
                    }}
                    numColumns={2}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        height: 80,
        backgroundColor: Color.primary,
        paddingHorizontal: 15
    },
    searchContainer: {
        borderRadius: 100,
        backgroundColor: Color.white,
        elevation: 2,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 8,
        position: 'absolute', bottom: -20,
        alignSelf: 'center',
    },
    search: {
        paddingVertical: 0,
        flex: 1,
    },
    category: {
        paddingHorizontal: 20, paddingVertical: 5,
        borderWidth: 1,
        borderRadius: 100,
        backgroundColor: 'transparent',
        marginRight: 10,
        borderColor: Color.primary
    },
    activeCategory: {
        paddingHorizontal: 20, paddingVertical: 5,
        borderWidth: 1,
        borderColor: Color.primary,
        borderRadius: 100,
        backgroundColor: Color.primaryOpacity,
        marginRight: 10,
    }
})