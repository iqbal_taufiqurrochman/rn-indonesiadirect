import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions, StatusBar } from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Button from '../component/Button';
import CartButton from '../component/CartButton';
import Header from '../component/Header';
import HorizontalMenu from '../component/HorizontalMenu';
import ProductDefault from '../component/Product/Default';
import Slider from '../component/Slider';
import store from '../redux/store';
import carting from '../util/carting';
import Color from '../util/Color';
import { connect } from 'react-redux';
import { setCart } from '../redux/actions';

const { width, height } = Dimensions.get('screen')
class ProductDetail extends Component {

    constructor(props) {
        super(props)
        this.state = {
            params: null,
            quantity: 1
        }
    }

    componentDidMount() {
        let params = this.props.route?.params?.item
        if (params) {
            this.setState({ params })
        }
    }

    addToCart() {
        let { quantity, params } = this.state
        params.quantity = quantity
        let cart = carting.process(params)
        this.props.dispatchCart(cart)
        this.props.navigation.navigate("Cart")
    }

    render() {
        let { params, quantity } = this.state
        return (
            <View style={{ flex: 1, backgroundColor: 'transparent' }}>
                <StatusBar backgroundColor={Color.primary} />
                <View style={styles.header}>
                    <TouchableOpacity activeOpacity={0.4} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back-outline" color={Color.white} size={25} />
                    </TouchableOpacity>

                    <Text style={{ color: Color.white, fontSize: 18, fontWeight: 'bold', marginHorizontal: 10 }}>Detail Produk</Text>
                </View>

                <View>
                    <SwiperFlatList
                        // autoplay
                        autoplayLoop
                        // showPagination
                        data={params?.images}
                        renderItem={({ item }) => (
                            <View style={{ height: 300, backgroundColor: Color.gray, elevation: 2, width: width }}>
                                <Image
                                    source={{ uri: item }}
                                    style={{ width: '100%', height: '100%' }}
                                    resizeMode="cover"
                                />
                            </View>
                        )}
                    />
                </View>

                <ScrollView
                    showsVerticalScrollIndicator={false}
                >

                    <Text style={{ paddingHorizontal: 15, paddingVertical: 10, fontWeight: 'bold', fontSize: 18, marginBottom: 10 }}>{params?.name}</Text>

                    <View style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                        <CartButton
                            value={quantity}
                            onChangeValue={(i) => {
                                if (i > 0) {
                                    this.setState({quantity : i})
                                }
                            }}
                        />
                        <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{params?.price_rupiah}</Text>
                    </View>

                    <View style={{ padding: 15, }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, marginBottom: 8 }}>Produk Detail</Text>
                        <Text style={{ marginBottom: 5 }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
                        <Text style={{ marginBottom: 5 }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
                    </View>
                </ScrollView>

                <Button
                    onPress={() => {
                        this.addToCart()
                    }}
                    style={{ height: 65, borderRadius: 0 }}
                >
                    Add to cart
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        height: 80,
        backgroundColor: Color.primary,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
})

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
    dispatchLogin: (data) => dispatch(login(data)),
    dispatchCart: (data) => dispatch(setCart(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductDetail);