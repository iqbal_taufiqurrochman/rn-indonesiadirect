import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Button from '../component/Button';
import Color from '../util/Color';

class MyAddress extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.navigation.addListener("focus", () => {
            this.getAllAddress()
        })
    }

    async getAllAddress() {
        let data = await AsyncStorage.getItem("@address")
        console.log("data", JSON.parse(data))
    }

    render() {
        return (
            <View style={styles.main}>
                <View style={styles.header}>
                    <TouchableOpacity activeOpacity={0.4} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back-outline" color={Color.white} size={25} />
                    </TouchableOpacity>

                    <Text style={{ color: Color.white, fontSize: 18, fontWeight: 'bold', marginHorizontal: 10 }}>My Address</Text>
                </View>

                <ScrollView>

                </ScrollView>

                <Button
                    onPress={() => this.props.navigation.navigate("AddAddress")}
                    style={{ height: 70, borderRadius: 0 }}>
                    Add Address
                </Button>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    header: {
        height: 80,
        backgroundColor: Color.primary,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
})

export default MyAddress