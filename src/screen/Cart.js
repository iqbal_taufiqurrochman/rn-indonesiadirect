import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions, TextInput, FlatList } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Button from '../component/Button';
import Header from '../component/Header';
import HorizontalMenu from '../component/HorizontalMenu';
import ProductDefault from '../component/Product/Default';
import ProductInCart from '../component/Product/InCart';
import Slider from '../component/Slider';
import store from '../redux/store';
import carting from '../util/carting';
import Color from '../util/Color';
import Rupiah from '../util/Rupiah';
import { connect } from 'react-redux';
import { setCart } from '../redux/actions';
import AsyncStorage from '@react-native-community/async-storage';

const category = [{}, {}, {}, {}, {}]
const { width, height } = Dimensions.get('screen')

class Cart extends Component {

    constructor(props) {
        super(props)
        this.state = {
            cart: [],
            totalTagihan: 0
        }
    }

    async componentDidMount() {
        this.reloadCart()
        // await AsyncStorage.setItem("@transactions", null)
    }

    reloadCart() {
        let currentCart = carting.getCurrentCart()
        this.setState({
            cart: currentCart
        }, this.hitungTotalTagihan)
        console.log("currentCart", currentCart)
    }

    hitungTotalTagihan() {
        let { cart } = this.state
        let total = 0
        if (cart) {
            cart.forEach((i) => {
                total += (i.quantity * i.price)
            })
            this.setState({ totalTagihan: total })
        }
    }

    async saveTransaction() {
        try {

            if (this.state.cart == null) {
                Alert.alert("Cart", "Maaf keranjang kosong")
                return
            }

            let orderSebelumnya = await AsyncStorage.getItem("@transactions")
            
            let data = {
                orders: this.state.cart,
                total: this.state.totalTagihan,
                alamat: "Gayungsari Timur 123, Surabaya",
                tanggal : new Date()
            }

            console.log({
                data: data,
                dataString: JSON.stringify(data)
            })

            if (orderSebelumnya == null) {
                let newOrder = []
                newOrder.push(data)
                await AsyncStorage.setItem("@transactions", JSON.stringify(newOrder))
            } else {
                let riwayat = JSON.parse(orderSebelumnya)
                riwayat.push(data)
                await AsyncStorage.setItem("@transactions", JSON.stringify(riwayat))
            }


            Alert.alert("Save Transaction", "Save transaction success")
            this.props.dispatchCart(null)
            this.props.navigation.popToTop()
            this.props.navigation.navigate("")
        } catch (error) {
            console.log("error", error)
        }
    }

    render() {

        let { navigations, cart } = this.props
        return (
            <View style={{ flex: 1, backgroundColor: 'transparent' }}>
                <View style={styles.header}>
                    <TouchableOpacity activeOpacity={0.4} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back-outline" color={Color.white} size={25} />
                    </TouchableOpacity>

                    <Text style={{ color: Color.white, fontSize: 18, fontWeight: 'bold', marginHorizontal: 10 }}>My Cart</Text>
                </View>

                <ScrollView>
                    <View style={{ paddingHorizontal: 15 }}>
                        <TouchableOpacity
                            disabled={true}
                            onPress={() => this.props.navigation.navigate("MyAddress")}
                        >
                            <View style={[styles.content, { flexDirection: 'row', alignItems: 'center' }]}>
                                <Ionicons name="location-sharp" size={25} color={Color.primary} />
                                <View style={{ marginHorizontal: 10 }}>
                                    <Text>Address</Text>
                                    <Text style={{ fontWeight: 'bold' }}>Gayungsari Timur 123, Surabaya</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, marginBottom: 10 }}>My Order</Text>
                            {this.state.cart == null && (
                                <Text style={{ margin: 20, fontSize: 14, fontStyle: 'italic', textAlign: 'center' }}>(Keranjang masih kosong)</Text>
                            )}
                            {this.state.cart != null && (
                                this.state.cart.map((item, index) => {
                                    return (
                                        <View key={index} style={styles.content}>
                                            <ProductInCart
                                                item={item}
                                                itemInCart={item.quantity}
                                                canRemoveItem={false}
                                                onChange={(i) => {
                                                    let result = carting.process(i)
                                                    this.props.dispatchCart(result)
                                                    this.reloadCart()
                                                }}
                                            />
                                        </View>
                                    )
                                })
                            )}
                        </View>
                    </View>
                </ScrollView>

                <View style={{ padding: 15 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, marginBottom: 10 }}>Payment Method</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 5 }}>
                        <Text>Cash</Text>
                        <Text>Change</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Total</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{Rupiah.format(this.state.totalTagihan)}</Text>
                    </View>
                </View>

                <Button
                    onPress={() => {
                        this.saveTransaction()
                    }}
                    style={{ height: 70, borderRadius: 0 }}
                    labelStyle={{ fontWeight: 'bold', fontSize: 20 }}
                >
                    Pay
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        height: 80,
        backgroundColor: Color.primary,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    content: {
        borderBottomWidth: 1.5,
        borderBottomColor: '#E5E5E5',
        paddingVertical: 10
    }
})

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
    dispatchLogin: (data) => dispatch(login(data)),
    dispatchCart: (data) => dispatch(setCart(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Cart);