import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert, Image, TextInput, StyleSheet } from 'react-native';
import { login } from '../redux/actions';
import { connect } from 'react-redux';
import { Container, Column, Row } from '../component/GridSystem';
import Button from '../component/Button';
import Color from '../util/Color';
import InputWithLabel from '../component/InputWithLabel';
import { HttpRequest } from '../util/Http';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            phone: '',
            screen: 'input-phone',
            otpCode: '',

            email: 'user@demo.com',
            password: '123456',
            users : []
        };

        this.parent = this.props.route.params.parent;
    }

    componentDidMount() {
        this.props.navigation.addListener("focus", () => {
            this.getHome()
        })
        this.getHome()
    }

    async getHome() {
        try {
            let res = await HttpRequest.getAllJson();
            console.log("res", res)
            this.setState({
                users: res.data.users,
            })
        } catch (error) {
            console.log(error)
            this.setState({ isLoading: false })
        }
    }

    login() {

        let { email, password, users } = this.state
        if (email == "") {
            Alert.alert("Email", "Mohon masukkan email dengan benar")
            return
        }
        if (password == "") {
            Alert.alert("Password", "Mohon masukkan password")
            return
        }

        let userExist = false
        userExist = users.some((i) => i.email == email && i.password == password)

        if (userExist) {
            //manggil fungsi di AppRouteConfig
            this.parent.saveAuth({ token: "Test" });
        } else {
            Alert.alert("Login", "Maaf, Email atau password anda salah")
            return
        }
    }

    requestOtp() {
        this.setState({ screen: 'input-code' });
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'transparent' }}>
                <View style={{ alignItems: 'center', }}>
                    <View style={styles.imageContainer}>
                        <Image source={require('../assets/logo.png')} style={{width : '70%', height : '70%'}} resizeMode="contain" />
                    </View>
                </View>

                <View style={{ marginHorizontal: 25, marginTop: 30 }}>
                    <InputWithLabel
                        label="Email"
                        placeholderTextColor={Color.gray}
                        placeholder="Email"
                        value={this.state.email}
                        onChangeText={(i) => this.setState({email : i})}
                    />
                    <InputWithLabel
                        label="Password"
                        placeholderTextColor={Color.gray}
                        placeholder="Password"
                        secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={(i) => this.setState({ password : i})}
                    />

                    <Button
                        onPress={() => this.login()}
                        style={{borderRadius : 13, height : 55, marginTop : 20}}
                    >Login</Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageContainer: {
        marginTop: 100,
        width: 150, height: 150,
        backgroundColor: Color.white, elevation: 2,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent : 'center'
    },
    input: {
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#DDD8D8',
        borderRadius: 12,
        marginBottom: 10,
        backgroundColor: Color.white,
    },
    rootCode: {
        flex: 1,
        justifyContent: 'center'
    },
    title: {
        color: Color.black,
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 20
    },
    image: {
        width: 300,
        height: 200
    },
    description: {
        color: Color.grayText,
        fontSize: 16,
        textAlign: 'center',
        fontFamily: "ProximaNovaRegular"
    },
    phoneWrapper: {
        backgroundColor: Color.white,
        paddingVertical: 10,
        elevation: 3,
        borderRadius: 10,
        alignItems: 'center'
    },
    phoneTitle: {
        color: Color.grayText,
        fontSize: 10,
        fontFamily: "ProximaNovaRegular"
    },
    phone: {
        fontSize: 20,
        color: Color.black,
    },
    otp: {
        fontSize: 50,
        color: Color.black,
        textAlign: 'center'
    }
});

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
    dispatchLogin: (data) => dispatch(login(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);