import { LOGIN, SET_CART, SET_ROOT } from "./actionTypes";

export function login(payload) {
    return { type: LOGIN, payload };
};

export function setRoot(payload) {
    return { type: SET_ROOT, payload };
};

export function setCart(payload) {
    return { type: SET_CART, payload };
};