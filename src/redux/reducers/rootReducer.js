import { LOGIN, SET_CART, SET_ROOT } from "../actionTypes";

const initialState = {
    user: null,
    root: null,
    cart: null
};

export default function (state = initialState, action) {
    let payload = action.payload;
    // console.log("Ac", action);
    switch (action.type) {
        case LOGIN: {
            state.user = payload;
            console.log("Login", payload);
            return state;
        }
        case SET_ROOT: {
            state.root = payload;
            return state;
        }
        case SET_CART: {
            state.cart = payload;
            return state;
        }
        default:
            return state;
    }
}
