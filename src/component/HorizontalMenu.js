import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';

class HorizontalMenu extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        let { label, items, containerStyle } = this.props
        return (
            <View style={[styles.main, containerStyle]}>

                {label && <Text style={{ fontSize: 20, fontWeight: 'bold', marginBottom: 10 }}>{label}</Text>}
                <ScrollView horizontal={true}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {items}
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        // flex: 1
    }
})

export default HorizontalMenu