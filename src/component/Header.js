import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Color from '../util/Color';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Header extends Component {
    render() {
        let leftIcon = 'arrow-left';
        let rightIcon = null;
        let textColor = Color.black;

        if (this.props.leftIcon) {
            leftIcon = this.props.leftIcon;
        }

        if (this.props.rightIcon) {
            rightIcon = this.props.rightIcon;
        }

        return (
            <View style={styles.root}>
                <TouchableOpacity activeOpacity={0.9} style={styles.navButton} onPress={() => {
                    // if (this.props.onLeftPress) {
                        this.props.onLeftPress();
                    // }
                }}>
                    <MaterialCommunityIcons name={leftIcon} size={30} color={textColor} />
                </TouchableOpacity>
                <Text style={styles.title}>{this.props.title}</Text>
                <TouchableOpacity activeOpacity={0.9} style={styles.navButton} onPress={() => {
                    if (this.props.onRightPress) {
                        this.props.onRightPress();
                    }
                }}>
                    {rightIcon && <MaterialCommunityIcons name={rightIcon} size={30} color={textColor} />}
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    root: {
        backgroundColor: Color.white,
        flexDirection: 'row',
        height: 50,
        alignItems: 'center'
    },
    navButton: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: Color.black,
        fontSize: 17,
        flex: 1,
        textAlign: 'center'
    },
    rightButton: {
        width: 50,
    }
};