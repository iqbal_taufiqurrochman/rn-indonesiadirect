import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Color from '../util/Color';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

class CartButton extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            quantity: 1
        }
    }

    componentDidMount() {
        if (this.props.value) {
            this.setState({ quantity: this.props.value })
        }
    }

    callback() {
        let { quantity } = this.state
        if (this.props.onChangeValue) {
            this.props.onChangeValue(quantity)
        }
    }

    render() {
        let { quantity } = this.state
        return (
            <View style={styles.main}>
                <TouchableOpacity
                    onPress={() => this.setState({ quantity: quantity - 1 }, this.callback)}
                >
                    <MaterialCommunityIcons name="minus" size={20} />
                </TouchableOpacity>

                <Text style={{ marginHorizontal: 15 }}>{quantity}</Text>

                <TouchableOpacity
                    onPress={() => this.setState({ quantity: quantity + 1 }, this.callback)}
                >
                    <MaterialCommunityIcons name="plus" size={20} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: Color.gray,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5
    }
})

export default CartButton