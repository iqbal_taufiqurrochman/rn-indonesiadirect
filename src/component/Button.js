import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Color from '../util/Color';

export default class Button extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.button, this.props.style]} onPress={() => {
                // if (this.props.onPress) {
                this.props.onPress();
                // }
            }}>
                <Text style={[styles.text, this.props.labelStyle]}>{this.props.children}</Text>
            </TouchableOpacity >
        );
    }
}

const styles = {
    button: {
        backgroundColor: Color.primary,
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        borderRadius: 5
    },
    text: {
        color: Color.white,
        fontSize: 17
    }
};