import React from 'react';
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native';
import Color from '../util/Color';

class InputWithLabel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isFocused: false,
        }
    }
    render() {
        let { label, labelStyle, inputStyle } = this.props
        let { isFocused } = this.state
        return (
            <View style={[styles.inputContainer,]}>
                {label && (<Text style={[styles.label, { labelStyle }]}>{label}</Text>)}
                <TextInput
                    {...this.props}
                    style={[styles.input, { inputStyle }]}
                    onFocus={() => this.setState({ isFocused: true })}
                    onBlur={() => this.setState({ isFocused: false })}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    label: {
        fontSize: 12,
        color: Color.gray,
        height: 15
        // marginHorizontal: 5
    },
    inputContainer: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderWidth: 1.5,
        borderColor: '#DDD8D8',
        borderRadius: 12,
        marginBottom: 10,
        backgroundColor: Color.white,
    },
    input: {
        paddingVertical: 0,
    }
})

export default InputWithLabel