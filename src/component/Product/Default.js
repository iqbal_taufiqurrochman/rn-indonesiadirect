import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView , Image} from 'react-native';
import Color from '../../util/Color';

class ProductDefault extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        let {
            label,
            item,
            containerStyle
        } = this.props

        let itemSize = 130
        if (this.props.itemSize) {
            itemSize = this.props.itemSize
        }
        // console.log("item", item)
        return (
            <TouchableOpacity
                activeOpacity={0.9}
                style={[styles.main, containerStyle]}
                onPress={this.props.onClick}>
                <View style={{ height: itemSize, width: itemSize, borderRadius: 8, backgroundColor: Color.gray, }}>
                    <Image {...this.props} style={{width : '100%', height : '100%'}} resizeMode="contain" />
                </View>

                <View style={{ marginTop: 10, marginLeft: 4, width : itemSize}}>
                    <Text numberOfLines={2} style={{ fontWeight: 'bold', fontSize: 15, marginBottom: 7 }}>{item?.name}</Text>
                    {/* <Text style={{ fontSize: 12 }}>{"3 Tons"}</Text> */}
                    <Text style={{ color: Color.primary }}>{item?.price_rupiah}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        // flex: 1
        alignItems: 'center',
        justifyContent : 'space-between'
    }
})

export default ProductDefault