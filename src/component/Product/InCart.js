import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import Color from '../../util/Color';
import CartButton from '../CartButton';

class ProductInCart extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            item: null
        }
    }

    componentDidMount() {
        if (this.props.item) {
            this.setState({ item: this.props.item })
        }
    }

    onChange() {
        let { item } = this.state
        if (this.props.onChange) {
            this.props.onChange(item)
        }
    }

    render() {
        let { item, itemInCart, editable = true } = this.props
        return (
            <View style={[styles.main, this.props.containerStyle]}>
                <View style={{ width: 68, height: 68, backgroundColor: Color.gray, borderRadius: 15 }}>
                    <Image
                        style={{ width: '100%', height: '100%', borderRadius: 5 }}
                        resizeMode="contain"
                        source={{ uri: item.images[0] }}
                    />
                </View>

                <View style={{ flex: 1, marginHorizontal: 8, marginRight: 20 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{item.name}</Text>
                    <Text style={{ fontSize: 14 }}>{item.price_rupiah}</Text>
                </View>

                {!editable && (
                    <Text>{itemInCart} x</Text>
                )}
                
                {editable && (
                    <CartButton
                        value={itemInCart}
                        onChangeValue={(i) => {
                            item.quantity = i
                            this.setState({ item }, this.onChange)
                        }}
                    />
                )}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        // flex : 1
        flexDirection: 'row',
        alignItems: 'center'
    }
})

export default ProductInCart