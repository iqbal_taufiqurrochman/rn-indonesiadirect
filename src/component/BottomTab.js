import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import Color from '../util/Color';

class BottomTab extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            menus: [
                { "label": "Home", page : "Home",icon: require('../assets/bottom_nav/home.png') },
                { "label": "Transaction", page: "Transaction",icon: require('../assets/bottom_nav/transaction.png') },
                { "label": "Cart", page : "Cart",icon: require('../assets/bottom_nav/cart.png') },
                { "label": "Profile", page : "Profile", icon: require('../assets/bottom_nav/profile.png') },
            ],
            selectedMenu: "Home"
        }
    }

    render() {
        let { menus, selectedMenu } = this.state
        let { selected } = this.props
        return (
            <DropShadow style={styles.dropShadow}>
                <View style={styles.main}>
                    {menus.map((item, key) => {
                        let isSelected = selected == key
                        return (
                            <TouchableOpacity
                                key={key}
                                onPress={() => this.props.onClick(item.page)}
                                style={styles.item}
                                activeOpacity={0.4}
                            >
                                <Image
                                    tintColor={isSelected ? Color.primary : 'normal'}
                                    source={item.icon}
                                    style={styles.icon} resizeMode="contain" />
                                <Text style={{ fontSize: 12, color : isSelected ? Color.primary : Color.black }}>{item.label}</Text>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </DropShadow>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
        paddingVertical: 10,
    },
    item: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        width: 35,
        height: 35
    },
    iconCart: {
        width: 25,
        height: 25
    },
    dropShadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -2,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        backgroundColor: '#fff',
        elevation: 3
    }
})

export default BottomTab