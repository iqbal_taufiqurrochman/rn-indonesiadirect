import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Dimensions } from 'react-native';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import Color from '../util/Color';
import { HttpRequest } from '../util/Http';

const colors = ['tomato', 'thistle', 'skyblue', 'teal'];
const { width, height } = Dimensions.get('screen')
class Slider extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            banner: []
        }
    }

    componentDidMount() {
        this.getBanner()
    }


    async getBanner() {
        try {
            let res = await HttpRequest.getAllJson()
            console.log("res", res.data)
            this.setState({
                banner: res.data.banner
            })
        } catch (error) {
            console.log("error", error)
        }
    }

    render() {
        let { banner } = this.state
        return (
            <View style={styles.main}>
                <SwiperFlatList
                    autoplay
                    autoplayLoop
                    // showPagination
                    data={banner}
                    renderItem={({ item }) => (
                        <View style={{ height: 180, marginHorizontal: 15, backgroundColor: Color.gray, elevation: 2, borderRadius: 20, width: width - 30 }}>
                            <Image
                                source={{ uri: item.image }}
                                style={{ width: '100%', height: '100%', borderRadius: 20 }}
                            />
                            <View style={{ position: 'absolute', padding: 15, bottom: 0, width: '100%', borderBottomRightRadius: 20, borderBottomLeftRadius: 20, backgroundColor: Color.blackOpacity }}>
                                <Text style={{ color: Color.white, fontSize: 20, fontWeight: 'bold' }}>{item?.title}</Text>
                            </View>
                        </View>
                    )}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        // flex : 1
    }
})

export default Slider