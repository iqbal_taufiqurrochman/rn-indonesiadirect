import store from "../redux/store";

export default {

    getCurrentCart() {
        let cart = store.getState().cart;
        return cart;
    },

    process(item) {
        let currentCart = this.getCurrentCart();
        let newCart = []

        if (currentCart) {
            let inCartIndex = currentCart.findIndex((i) => i.id === item.id)
            if (inCartIndex != -1) {

                if (item.quantity <= 0) {
                    // remove item when quantity <= 0
                    currentCart.splice(inCartIndex, 1)
                } else {
                    // update item in cart
                    currentCart[inCartIndex] = item
                }

            } else {
                if (item.quantity != 0) {
                    // add to cart
                    currentCart.push(item)
                }
            }
        } else {
            // add new item to cart
            newCart.push(item)
        }

        return currentCart ?? newCart;
    },

    isItemInCart(item) {
        let currentCart = this.getCurrentCart();
        if (currentCart) {
            let inCartIndex = currentCart.findIndex((i) => i.id === item.id)
            if (inCartIndex != -1) {
                return currentCart[inCartIndex].quantity
            } else {
                return 0;
            }
        }
    },

    grandTotalItemCart() {
        let currentCart = this.getCurrentCart();
        let grandTotal = 0
        if (currentCart) {
            currentCart.forEach(element => {
                console.log("element : ", element)
                if (element.quantity != 0) {
                    grandTotal += (element.quantity * element.price)
                }
            });
            return grandTotal
        }
        return grandTotal
    },

    totalItemInCart() {
        let currentCart = this.getCurrentCart();
        if (currentCart) {
            return currentCart.length;
        } else {
            return 0;
        }
    }
}