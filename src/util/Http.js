import axios from "axios";

const BASE_URL = "https://my-json-server.typicode.com/iiqbalt/IndoDirect";
const BASE_URL_JSON = "https://iiqbalt.github.io/indodirect.json";

export const HttpRequest = {
    
    getProduct() {
        return axios.get(BASE_URL + "/products")
    },
    getCategory() {
        return axios.get(BASE_URL + "/category")
    },
    getBanner() {
        return axios.get(BASE_URL + "/banner")
    },

    getAllJson() {
        return axios.get(BASE_URL_JSON)
    }
}