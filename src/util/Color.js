export default {
    primary: '#FD191D',
    primaryOpacity: 'rgba(253, 25, 29, 0.2)',
    white: '#fff',
    black: '#000',
    blackOpacity: 'rgba(0,0,0,0.4)',
    grayText: '#555',
    gray: '#7f8c8d'
}