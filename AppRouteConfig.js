import * as React from 'react';
import { View, Text, ActivityIndicator, StatusBar, Image } from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

import { createStackNavigator } from '@react-navigation/stack';

import Home from './src/screen/Home';
import store from './src/redux/store';
import Login from './src/screen/Login';
import { login, setRoot } from './src/redux/actions';
import Color from './src/util/Color';
import Cari from './src/screen/Cari';
import ProductDetail from './src/screen/ProductDetail';
import Cart from './src/screen/Cart';
import MyAddress from './src/screen/MyAddress';
import AddAddress from './src/screen/AddAddress';
import Transaction from './src/screen/Transaction';

const Stack = createStackNavigator();

class AppRouteConfig extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true
        };
    }

    componentDidMount() {
        this.props.dispatchRoot(this);

        setTimeout(() => {
            //load asyncstorage
            this.loadAuth();
        }, 2500);
    }

    loadAuth() {
        this.setState({ isLoading: true });
        AsyncStorage.getItem('@user_token').then((result) => {
            let data = JSON.parse(result);
            console.log("Data Loaded", data);
            this.saveAuth(data);
        }).catch((err) => {
            this.setState({ isLoading: false });
        })
    }

    saveAuth(value) {
        if (value) {
            AsyncStorage.setItem('@user_token', JSON.stringify(value));
        } else {
            AsyncStorage.removeItem('@user_token');
        }

        this.props.dispatchLogin(value);
        this.setState({ isLoading: false });
    }

    render() {
        if (!this.state.isLoading) {
            let data = store.getState();
            if (data.user) {
                return (
                    <Stack.Navigator screenOptions={{ headerShown: false }}>
                        <Stack.Screen name="Home" component={Home} />
                        <Stack.Screen name="Cari" component={Cari} />
                        <Stack.Screen name="ProductDetail" component={ProductDetail} />
                        <Stack.Screen name="Cart" component={Cart} />
                        <Stack.Screen name="MyAddress" component={MyAddress} />
                        <Stack.Screen name="AddAddress" component={AddAddress} />
                        <Stack.Screen name="Transaction" component={Transaction} />
                    </Stack.Navigator>
                );
            } else {
                return (
                    <Stack.Navigator screenOptions={{ headerShown: false }}>
                        <Stack.Screen name="Login" component={Login}
                            initialParams={{ parent: this }} />
                    </Stack.Navigator>
                );
            }
        }

        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: Color.white }}>
                <Image
                    style={{ width : 200, height : 200,  backgroundColor : Color.white, padding: 20,  }} resizeMode="contain"
                    source={require('./src/assets/logo.png')} />
                <ActivityIndicator size='large' color={Color.primary} />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
    dispatchLogin: (data) => dispatch(login(data)),
    dispatchRoot: (data) => dispatch(setRoot(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AppRouteConfig);